# Detection list
Here's the list of types of malware SpigotAV can detect:<br>
Worm: Spreads to other plugins/systems<br>
Backdoor: Can execute code/commands sent by a third party<br>
PUL: Potentially unwanted library, with no reasonable legitimate uses

Name: worm.backdoor.Rafael10<br>
Notable: .l1, .l_ignore and javassist in root of jar

Name: PUL.javassist<br>
Notable: javassist in root of jar, library for java bytecode manipulation (modifying jars, e.g. injecting code)