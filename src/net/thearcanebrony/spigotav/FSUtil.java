package net.thearcanebrony.spigotav;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class FSUtil {
    public static List<String> findAllJars(String path, boolean recursive) throws IOException {
        List<String> jars = new ArrayList<String>();
        int i = 0;
        for(Path p : Files.list(Path.of(path)).filter(x -> x.toString().endsWith(".jar")).toArray(Path[]::new)){
            jars.add(p.toString());
            i++;
        }
        if(i>0) System.out.println("Found " + i + " jar files in "+path+"!");
        if(recursive){
            for(Path dir : Files.list(Path.of(path)).filter(Files::isDirectory).filter(path1 -> !Files.isSymbolicLink(path1)).toArray(Path[]::new)) {
                try{
                    jars.addAll(findAllJars(dir.toString(), true));
                } catch(Exception e) {
                    ConUtil.println(new RGBColor(255,0,0) + "Failed to scan " + dir + "!" + new RGBColor(255,255,255));
                }
            }

        }
        return jars;
    }
}
