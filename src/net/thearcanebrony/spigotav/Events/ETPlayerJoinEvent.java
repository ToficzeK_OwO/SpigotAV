package net.thearcanebrony.spigotav.Events;

import net.thearcanebrony.spigotav.Config;
import net.thearcanebrony.spigotav.HumanInterface;
import net.thearcanebrony.spigotav.wrappers.ETEventHandler;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class ETPlayerJoinEvent extends ETEventHandler {
    @EventHandler
    public static void HandleEvent(org.bukkit.event.player.PlayerJoinEvent e) {
        HandlePlayerJoin(e.getPlayer());
    }

    public static void HandlePlayerJoin(Player p) {
        if( Config.WelcomeMessageEnabled) p.sendMessage(Config.WelcomeMessage);
        for (String cmd : getConfig().getStringList("Commands On Join")) p.performCommand(cmd);
        for (String cmd : getConfig().getStringList("Player Data." + p.getName() + ".Commands On Join"))
            p.performCommand(cmd);
        for (String cmd : getConfig().getStringList("Console Commands On Join")) p.performCommand(cmd);
        for (String cmd : getConfig().getStringList("Player Data." + p.getName() + ".Console Commands On Join"))
            p.performCommand(cmd);
        if(Config.SendEveryone){
            HumanInterface.SendResultsToPlayer(p);
            HumanInterface.ShowTitle(p);
        } else if(Config.SendWithPerm) {
            if(p.hasPermission("spigotav.viewwarns")) {
                HumanInterface.SendResultsToPlayer(p);
                HumanInterface.ShowTitle(p);
            }
        } else if(Config.SendOPs){
            if(p.isOp()) {
                HumanInterface.SendResultsToPlayer(p);
                HumanInterface.ShowTitle(p);
            }
        }
    }
}

