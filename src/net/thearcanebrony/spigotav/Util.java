package net.thearcanebrony.spigotav;

import net.thearcanebrony.spigotav.datastorage.ScanResult;

import java.util.Comparator;
import java.util.Optional;

public class Util {
    private static int maxSrcLength = 0;
    public static void logDebug(String text){
        if(RuntimeStore.LogDebug) {
            StackTraceElement ste = Thread.currentThread().getStackTrace()[2];
            String[] lines = text.split("\n");
            String source = ste.getFileName() + ":" + ste.getLineNumber();
            if(source.length() >= maxSrcLength)
                maxSrcLength = source.length();
            else while (source.length() < maxSrcLength)
                source = source + " ";
            for (String line : lines) System.out.println(ste.getFileName() + ":" + ste.getLineNumber() + " -> " + line);
        }
    }

    //source: https://stackoverflow.com/questions/140131/convert-a-string-representation-of-a-hex-dump-to-a-byte-array-using-java
    public static int[] hexStringToByteArray(String s) {
        int len = s.length();
        int[] data = new int[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }
    public static int getHighestDetectionLevel(){
        Optional<ScanResult> res = RuntimeStore.ScanResults.stream().max(Comparator.comparingInt(x->x.getSeverity()));
        if(res.isPresent()) return res.get().getSeverity();
        return 0;
    }
}
