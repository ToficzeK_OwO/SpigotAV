package net.thearcanebrony.spigotav.wrappers;

import net.thearcanebrony.spigotav.Main;
import org.bukkit.Server;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.Listener;

public class ETEventHandler implements Listener {
    public static FileConfiguration getConfig() {
        return Main.getPlugin().getConfig();
    }
    public static Server getServer() { return Main.getPlugin().getServer(); }
}
