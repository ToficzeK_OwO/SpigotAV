package net.thearcanebrony.spigotav;

public class ConUtil {
    public static void println(String text){
        System.out.println(translateColors(text));
    }
    private static String translateColors(String text){
        return RGBColor.ColorPattern.matcher(text).replaceAll(matchResult -> new RGBColor(matchResult.group(0)).getAnsiColor());
    }
}
