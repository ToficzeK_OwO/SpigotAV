package net.thearcanebrony.spigotav.datastorage;

import net.thearcanebrony.spigotav.RGBColor;

public class ScanResult {
    public String File = "";
    public boolean Suspicious = false;
    public String Detection = "";

    public ScanResult(String file, boolean sus, String det) {
        File = file;
        Suspicious = sus;
        Detection = det;
    }

    public static RGBColor getColor(int i) {
        switch (i) {
            case Severity.DANGER:
                return new RGBColor(255, 0, 0);
            case Severity.SUSPICIOUS_LIB:
                return new RGBColor(255, 127, 255);
            case Severity.WARNING:
                return new RGBColor(255, 127, 0);
            case Severity.SAFE:
                return new RGBColor(0, 255, 0);
            case Severity.UNKNOWN:
            default:
                return new RGBColor(127, 127, 127);
        }
//        return StaticData.DetectionColors[i];
    }

    public int getSeverity() {
        switch (Detection) {
            case "worm.backdoor.Rafael10":
                return Severity.WARNING;
            case "pul.javassist":
                return Severity.SUSPICIOUS_LIB;
            case "corrupted_jar":
                return Severity.UNKNOWN;
            default:
                return 0;
        }
    }

    public RGBColor getColor() {
        return getColor(getSeverity());
    }

    public static class Severity {
        public static final int UNKNOWN = -1;
        public static final int SAFE = 0;
        public static final int SUSPICIOUS_LIB = 1;
        public static final int WARNING = 2;
        public static final int DANGER = 3;
    }
}
